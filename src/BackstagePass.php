<?php

namespace src;

final class BackstagePass extends ItemDecorator
{
    const MIN_QUALITY = 0;
    const MAX_QUALITY_TO_INCREASE = 3;
    const EXTRA_QUALITY_TO_INCREASE = 2;
    const LAST_SELLIN_PERIOD = 5;
    const PENULTIMATE_SELLIN_PERIOD = 10;

    public function update()
    {
        $this->updateSellIn();
        if (!$this->isStillForSale()) {
            $this->item->setQuality(self::MIN_QUALITY);
        } elseif ($this->inLastSellInPeriod()) {
            $this->increaseQuality(self::MAX_QUALITY_TO_INCREASE);
        } elseif ($this->inPenultimateSellInPeriod()) {
            $this->increaseQuality(self::EXTRA_QUALITY_TO_INCREASE);
        } else {
            $this->increaseQuality(self::STANDARD_QUALITY_TO_INCREASE);
        }
    }

    private function inLastSellInPeriod()
    {
        return $this->item->getSellIn() <= self::LAST_SELLIN_PERIOD;
    }

    private function inPenultimateSellInPeriod()
    {
        return $this->item->getSellIn() <= self::PENULTIMATE_SELLIN_PERIOD;
    }
}

