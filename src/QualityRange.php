<?php

namespace src;

class QualityRange
{
    const MAX_QUALITY = 50;
    const MIN_QUALITY = 0;

    private function isQualityInRange($amount)
    {
        return ($amount >= self::MIN_QUALITY && $amount <= self::MAX_QUALITY);
    }

    private function isQualityTooLow($amount)
    {
        return ($amount < self::MIN_QUALITY);
    }

    public function adjustQuality($amount)
    {
        if ($this->isQualityInRange($amount)) {
            return $amount;
        } elseif ($this->isQualityTooLow($amount)) {
            return self::MIN_QUALITY;
        } else {
            return self::MAX_QUALITY;
        }
    }
}

