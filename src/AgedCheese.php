<?php

namespace src;

final class AgedCheese extends ItemDecorator
{
    public function update()
    {
        $this->updateSellIn();
        $this->increaseQuality(self::STANDARD_QUALITY_TO_INCREASE);
    }
}

