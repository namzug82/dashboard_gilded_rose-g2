<?php

namespace src;

final class StandardItem extends ItemDecorator
{
    public function update()
    {
        $this->updateSellIn();
        if ($this->isStillForSale()) {
            $this->decreaseQuality(self::STANDARD_QUALITY_TO_DECREASE);
        } else {
            $this->decreaseQuality(self::EXTRA_QUALITY_TO_DECREASE);
        }
    }
}

