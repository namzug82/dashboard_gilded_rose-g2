<?php

namespace src;

abstract class ItemDecorator extends \Item
{
    const MIN_SELLIN_VALUE = 0;
    const DAILY_SELLIN_TO_DECREASE = 1;
    const STANDARD_QUALITY_TO_DECREASE = 1;
    const EXTRA_QUALITY_TO_DECREASE = 2;
    const STANDARD_QUALITY_TO_INCREASE = 1;

    protected $item;

    public function __construct(\Item $itemToDecorate)
    {
        $this->item = $itemToDecorate;
    }

    abstract public function update();

    final protected function updateSellIn()
    {
        $current_sellIn = $this->item->getSellIn();
        $this->item->setSellIn($current_sellIn - static::DAILY_SELLIN_TO_DECREASE);
    }

    final protected function isStillForSale()
    {
        return $this->item->getSellIn() >= static::MIN_SELLIN_VALUE;
    }

    final protected function increaseQuality($quantity_to_increase)
    {
        $modified_quality = $this->item->getQuality() + $quantity_to_increase;
        $this->updateQuality($modified_quality);
    }

    final protected function decreaseQuality($quantity_to_decrease)
    {
        $modified_quality = $this->item->getQuality() - $quantity_to_decrease;
        $this->updateQuality($modified_quality);
    }

    private function updateQuality($new_quality)
    {
        $quality = new QualityRange();
        $adjustedQuality = $quality->adjustQuality($new_quality);
        $this->item->setQuality($adjustedQuality);
    }
}

