<?php
error_reporting(E_ALL | E_STRICT);
ini_set("display_errors", "on");

include_once __DIR__ .'/autoload.php';

class GildedRose
{
    public static function updateQuality(array $items)
    {
        foreach ($items as $current_item) {
            $decorated_item = src\ItemFactory::getItem($current_item);
            $decorated_item->update();
        }
    }
}

