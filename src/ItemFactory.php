<?php

namespace src;

class ItemFactory
{
    const AGED_CHEESES = "Aged Brie";
    const LEGENDARY_ITEMS = "Sulfuras, Hand of Ragnaros";
    const BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";

    public static function getItem(\Item $item)
    {
        $item_name = $item->getName();
        switch ($item_name) {
            case self::AGED_CHEESES:
                return new AgedCheese($item);
                break;
            case self::LEGENDARY_ITEMS:
                return new LegendaryItem($item);
                break;
            case self::BACKSTAGE_PASSES:
                return new BackstagePass($item);
                break;
            default:
                return new StandardItem($item);
                break;
        }
    }
}
